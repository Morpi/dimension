// add and delete class .is-load
// must be out JQuery $(document).ready function
$('body').addClass('is-loading');

$(window).on('load', function() {
	window.setTimeout(function() {
		$('body').removeClass('is-loading');
	}, 100);
});

$(document).ready(function() {
	// open $this section menu
	$('.header_button li a').on('click', function() {
		var $id = $(this).attr('href');
		$('#header, #footer').fadeOut(300, function(){
			$('#header').css({'display' : 'none'});
			$('#footer').css({'display' : 'none'});
			$('body').addClass('is-section-visible');

			$('#main').effect('slide', {direction: 'up'}, 300, function(){
				$('#main').css({'display' : 'flex'});
				$($id).css({'display' : 'flex'});
			});
		});
	});

	// reopen new section
	$('section a').on('click', function() {
		var $hideId = $('section').attr('id');
		$('#' + $hideId).css({'display' : 'none'});
		var $showId = $(this).attr('href');
		$($showId).css({'display' : 'flex'});
	});

	// add close button
	$('.close_button').on('click', function() {
		$id = $(this).parent();
		$('#main').effect('slide', {direction: 'up', mode: 'hide'}, 300, function(){
			$($id).css({'display' : 'none'});
			$('#main').css({'display' : 'none'});
			$('body').removeClass('is-section-visible');
			$('#header, #footer').fadeIn(300, function(){
				$('#header').css({'display' : 'flex'});
				$('#footer').css({'display' : 'flex'});
			});
		});
	});

	// close section when click out window
	$(document).mouseup(function (e){
		var div = $("section");
		if (!div.is(e.target) && div.has(e.target).length === 0) {
			$('#main').effect('slide', {direction: 'up', mode: 'hide'}, 300, function(){
				div.hide();
				$('#main').css({'display' : 'none'});
				$('body').removeClass('is-section-visible');
				$('#header, #footer').fadeIn(300, function(){
					$('#header').css({'display' : 'flex'});
					$('#footer').css({'display' : 'flex'});
				});
			});
		}
	});
})